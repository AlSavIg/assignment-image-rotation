file(GLOB_RECURSE sources CONFIGURE_DEPENDS
    src/*.c
    src/*.h
    include/*.h
)

add_executable(image-transformer ${sources} src/err_manager.c include/err_message.h include/err_status.h src/bmp_io.c)
target_include_directories(image-transformer PRIVATE src include)
