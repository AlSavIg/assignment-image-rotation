#include "bmp_io.h"
#include "file_open_close.h"
#include "rotation.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    enum args_status checkArgs = check_args(argc);
    if (checkArgs != ARGS_OK) exit(checkArgs);

    struct opened_file inputFile = open_file(argv[1], READ_BYTE);
    struct opened_file outputFile = open_file(argv[2], WRITE_BYTE);
    FILE *input_fp = inputFile.fp;
    FILE *output_fp = outputFile.fp;
    if (inputFile.openStatus != 0) exit(inputFile.openStatus);
    if (outputFile.openStatus != 0) exit(outputFile.openStatus);

    struct image_traceback imageTraceback = from_bmp(input_fp);
    if ( imageTraceback.readStatus != READ_OK ) exit(imageTraceback.readStatus);

    struct image image = imageTraceback.image;
    struct image rotated_image;

    enum rotate_status rotateStatus = rotate_image(&image, &rotated_image);
    if (rotateStatus != ROTATE_OK) exit(rotateStatus);

    enum write_status writeStatus = to_bmp(output_fp, &rotated_image);
    if (writeStatus != WRITE_OK) exit(writeStatus);

    enum close_status closeStatus = close_file(input_fp, argv[1]);
    if (closeStatus != CLOSE_OK) exit(CLOSE_ERROR);
    closeStatus = close_file(output_fp, argv[2]);
    if (closeStatus != CLOSE_OK) exit(CLOSE_ERROR);

    free(image.data);  // - THROWS SEGFAULT
    free(rotated_image.data);

    return 0;
}
