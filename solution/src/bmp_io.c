//
// Created by User on 27.01.2023.
//
#include "bmp_io.h"
#include "err_message.h"
#include "err_status.h"
#include "image_struct.h"
#include "utils.h"
#include <stdint.h>
#include <stdio.h>

static uint32_t get_padding(const uint32_t width, const uint32_t multiplicity) {
    return (uint32_t) (multiplicity - width * sizeof(struct pixel)
                                      % multiplicity) % multiplicity;
}

static struct bmp_header recover_bmp_headers(const struct image * const image) {
    uint64_t height = image -> height;
    uint64_t width = image -> width;
    struct bmp_header header = {
            .biSize = 40,
            .bfType = 19778,
            .biPlanes = 1,
            .bfileSize = height * width * sizeof(struct pixel)
                         + height * get_padding(width, 4) + sizeof(struct bmp_header),
            .biHeight = height,
            .biWidth = width,
            .biBitCount = 24,
            .bOffBits = sizeof(struct bmp_header)
    };
    return header;
}

static enum write_status write_headers(FILE * const fp, const struct bmp_header * const headers) {
    uint32_t struct_count = 1;
    if (fwrite(headers,
               sizeof(struct bmp_header),
               struct_count,
               fp) != struct_count) {
        fprintf(stderr, "[ERROR] %s\n",
                get_write_status(HEADERS_WRITE_ERROR));
        return HEADERS_WRITE_ERROR;
    }
    return WRITE_OK;
}

static enum write_status write_pixels(FILE * const fp, const struct image * const image) {
    uint64_t height = image -> height;
    uint64_t width = image -> width;
    char nulls[3] = {'0'};
    uint32_t padding = get_padding(width, 4);
    for (uint32_t line_num = 0; line_num < height; line_num++) {
        if (fwrite(image -> data + line_num * width,
                   sizeof(struct pixel),
                   width,
                   fp) != width || fwrite(nulls,
                                          sizeof(char),
                                          padding,
                                          fp) != padding) {
            fprintf(stderr, "[ERROR] %s\n",
                    get_write_status(PIXELS_WRITE_ERROR));
            return PIXELS_WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE * const out, const struct image * const img) {
    struct bmp_header headers = recover_bmp_headers(img);
    enum write_status writeStatus;

    writeStatus = write_headers(out,&headers);
    if (writeStatus != 0) return writeStatus;

    writeStatus = write_pixels(out, img);
    if (writeStatus != 0) return writeStatus;

    printf("[INFO] %s\n",
           get_write_status(WRITE_OK));
    return WRITE_OK;
}

static enum read_status read_bmp_headers(struct bmp_header * const headers, FILE * const fp) {
    size_t structs_count = 1;
    size_t struct_size = sizeof(struct bmp_header);
    if (fread(headers,
              struct_size,
              structs_count,
              fp) != structs_count) {
        fprintf(stderr, "[ERROR] %s\n",
                get_read_status(READ_INVALID_HEADER));
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}

static enum read_status read_bmp_pixels(const struct image * const image,
                                        FILE * const fp,
                                        const uint32_t padding) {
    size_t pixel_size = sizeof(struct pixel);
    uint64_t height = image -> height;
    uint64_t width = image -> width;
    for (uint64_t i = 0; i < height; i++) {
        if (fread(image -> data + i * width,
                  pixel_size,
                  width,
                  fp) == 0) {
            fprintf(stderr, "[ERROR] %s\n",
                    get_read_status(READ_INVALID_PIXELS));
            return READ_INVALID_PIXELS;
        }
        if (fseek(fp, (long) padding, SEEK_CUR) != 0) {
            fprintf(stderr, "[ERROR] %s\n",
                    get_read_status(READ_INVALID_SEEK));
            return READ_INVALID_SEEK;
        }
    }
    printf("[INFO] %s\n", get_read_status(READ_OK));
    return READ_OK;
}

struct image_traceback from_bmp(FILE * const in) {
    struct image image;
    struct bmp_header headers;

    enum read_status readStatus = read_bmp_headers(&headers, in);
    if (readStatus != READ_OK) {
        return (struct image_traceback) {.readStatus = readStatus};
    }

    enum dynamic_memory_status dynamicMemoryStatus = image_init(&image,
                                                                headers.biHeight,
                                                                headers.biWidth);
    if ( dynamicMemoryStatus != MALLOC_OK ) {
        return (struct image_traceback) {.readStatus = READ_ERROR};
    }

    readStatus = read_bmp_pixels(&image, in, get_padding(headers.biWidth,4));
    if (readStatus != READ_OK) {
        return (struct image_traceback) {.readStatus = readStatus};
    }

    return (struct image_traceback) {.image = image, .readStatus = READ_OK};
}
