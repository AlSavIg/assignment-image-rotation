//
// Created by alexander on 10/23/22.
//

#include "file_open_close.h"
#include "err_message.h"
#include "err_status.h"
#include <stdio.h>
#include <stdlib.h>

static const char * const open_mode_char[] = {
        "r",
        "rb",
        "w",
        "wb",
        "a",
        "ab",
        "r+",
        "w+",
        "a+",
};

struct opened_file open_file(const char * const f_name, enum open_mode mode) {
    FILE *fp = NULL;
    if ((fp = fopen(f_name, open_mode_char[mode])) == NULL) {
        fprintf(stderr, "[ERROR] %s %s\n",
                get_open_status(OPEN_ERROR),
                f_name);
        return (struct opened_file) {.fp = fp, .openStatus = OPEN_ERROR};
    }
    printf("[INFO] %s %s\n",
           get_open_status(OPEN_OK),
           f_name);
    return (struct opened_file) {.fp = fp, .openStatus = OPEN_OK};;
}

enum close_status close_file(FILE * const f, const char * const f_name) {
//    TROWS SEGFAULT in case file not found
    if (fclose(f) != 0) {
        fprintf(stderr, "[ERROR] %s %s\n",
                get_close_status(CLOSE_ERROR),
                f_name);
        return CLOSE_ERROR;
    }
    printf("[INFO] %s %s\n",
           get_close_status(CLOSE_OK),
           f_name);
    return CLOSE_OK;
}
