//
// Created by User on 27.01.2023.
//

#include "err_status.h"

static const char * const read_status_names[] = {
        "READ_SUCCESS",
        "READ_INVALID_HEADERS",
        "READ_INVALID_PIXELS",
        "SEEK FAILED",
        "READ_ERROR",
};

const char *get_read_status(enum read_status status) {
    return read_status_names[status];
}

static const char * const write_status_names[] = {
        "WRITE_SUCCESS",
        "HEADERS_WRITE_ERROR",
        "PIXELS_WRITE_ERROR",
};

const char *get_write_status(enum write_status status) {
    return write_status_names[status];
}

static const char * const open_status_names[] = {
        "OPEN_SUCCESS",
        "OPEN_ERROR",
};

const char *get_open_status(enum open_status status) {
    return open_status_names[status];
}

static const char * const close_status_names[] = {
        "CLOSE_SUCCESS",
        "CLOSE_ERROR",
};

const char *get_close_status(enum close_status status) {
    return close_status_names[status];
}

static const char * const rotate_status_names[] = {
        "IMAGE SUCCESSFULLY ROTATED",
        "NEW IMAGE DATA IS EMPTY",
};

const char *get_rotate_status(enum rotate_status status) {
    return rotate_status_names[status];
}

static const char * const args_status_names[] = {
        "ARGS_ARE_CORRECT",
        "THERE ARE MUST BE MORE/LESS ARGS",
};

const char *get_args_status(enum args_status status) {
    return args_status_names[status];
}

static const char * const dynamic_memory_status_names[] = {
        "MEMORY ALLOCATED SUCCESSFULLY",
        "MEMORY ERROR",
};

const char *get_dynamic_memory_status(enum dynamic_memory_status status) {
    return dynamic_memory_status_names[status];
}
