//
// Created by alexander on 10/23/22.
//

#include "err_message.h"
#include "err_status.h"
#include "image_struct.h"
#include "rotation.h"
#include "utils.h"
#include <stdint.h>
#include <stdio.h>

enum rotate_status rotate_image(struct image * const old_image, struct image * const new_image) {
    uint64_t old_height = old_image -> height;
    uint64_t old_width = old_image -> width;
    struct pixel *old_data = old_image -> data;

    enum dynamic_memory_status dynamicMemoryStatus = image_init(new_image, old_width, old_height);
    if (dynamicMemoryStatus != 0) return ROTATE_ERROR;

    uint64_t new_height = new_image -> height;
    uint64_t new_width = new_image -> width;
    struct pixel *new_data = new_image -> data;

    for (uint64_t i = 0; i < new_width; i++) {
        for (uint64_t j = 0; j < new_height; j++) {
            new_data[j * new_width + new_width - 1 - i] = old_data[i * new_height + j];
        }
    }

    if (new_data == NULL) {
        fprintf(stderr, "[ERROR] %s\n",
                get_rotate_status(ROTATE_ERROR));
        return ROTATE_ERROR;
    }

    printf("[INFO] %s\n",
           get_rotate_status(ROTATE_OK));
    return ROTATE_OK;
}
