//
// Created by alexander on 10/23/22.
//

#include "err_message.h"
#include "err_status.h"
#include "utils.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum args_status check_args(int argc) {
    if (argc != 3) {
        fprintf(stderr, "[ERROR] %s\n",
                get_args_status(WRONG_ARGS));
        return WRONG_ARGS;
    }
    printf("[INFO] %s\n", get_args_status(ARGS_OK));
    return ARGS_OK;
}

enum dynamic_memory_status image_init(struct image * const image, const uint64_t height, const uint64_t width) {
    image -> height = height;
    image -> width = width;
    if ((image -> data = malloc(sizeof(struct pixel) * height * width)) == NULL) {
        fprintf(stderr, "[ERROR] %s\n",
                get_dynamic_memory_status(MALLOC_ERROR));
        return MALLOC_ERROR;
    }
    printf("[INFO] %s\n",
           get_dynamic_memory_status(MALLOC_OK));
    return MALLOC_OK;
}
