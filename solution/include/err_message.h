//
// Created by User on 27.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ERR_MESSAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_ERR_MESSAGE_H

#include "err_status.h"

const char *get_read_status(enum read_status status);

const char *get_write_status(enum write_status status);

const char *get_open_status(enum open_status status);

const char *get_close_status(enum close_status status);

const char *get_rotate_status(enum rotate_status status);

const char *get_args_status(enum args_status status);

const char *get_dynamic_memory_status(enum dynamic_memory_status status);

#endif //ASSIGNMENT_IMAGE_ROTATION_ERR_MESSAGE_H
