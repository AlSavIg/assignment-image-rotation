//
// Created by alexander on 10/23/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_IO_H

#include "err_status.h"
#include "image_struct.h"
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)

struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#pragma pack(pop)

struct image_traceback {
    struct image image;
    enum read_status readStatus;
};

struct image_traceback from_bmp(FILE* in);
enum write_status to_bmp(FILE* out, const struct image *img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_IO_H
