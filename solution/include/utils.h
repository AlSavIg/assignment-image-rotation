//
// Created by alexander on 10/23/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_UTIL_H
#define ASSIGNMENT_IMAGE_ROTATION_UTIL_H

#include "err_status.h"
#include "image_struct.h"
#include <stdint.h>

enum args_status check_args(int argc);
enum dynamic_memory_status image_init(struct image *image, uint64_t height, uint64_t width);

#endif //ASSIGNMENT_IMAGE_ROTATION_UTIL_H
