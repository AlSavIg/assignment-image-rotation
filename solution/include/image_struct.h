//
// Created by alexander on 10/23/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_STRUCT_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_STRUCT_H

#include <stdint.h>

struct pixel{
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_STRUCT_H
