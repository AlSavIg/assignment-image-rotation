//
// Created by alexander on 10/23/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "err_status.h"
#include "image_struct.h"

enum rotate_status rotate_image(struct image *old_image, struct image *new_image);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
