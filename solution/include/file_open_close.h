//
// Created by alexander on 10/23/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H

#include "err_status.h"
#include <stdio.h>

enum open_mode {
    READ = 0,
    READ_BYTE,
    WRITE,
    WRITE_BYTE,
    ADD,
    ADD_BYTE,
    READ_PLUS,
    WRITE_PLUS,
    ADD_PLUS,
};

struct opened_file {
    FILE *fp;
    enum open_status openStatus;
};

struct opened_file open_file(const char * f_name, enum open_mode mode);
enum close_status close_file(FILE *f, const char * f_name);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H
