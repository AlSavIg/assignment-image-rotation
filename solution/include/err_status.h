//
// Created by User on 27.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ERR_STATUS_H
#define ASSIGNMENT_IMAGE_ROTATION_ERR_STATUS_H

enum write_status  {
    WRITE_OK = 0,
    HEADERS_WRITE_ERROR,
    PIXELS_WRITE_ERROR,
};

enum read_status  {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_PIXELS,
    READ_INVALID_SEEK,
    READ_ERROR,
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR,
};

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR,
};

enum rotate_status {
    ROTATE_OK = 0,
    ROTATE_ERROR,
};

enum dynamic_memory_status {
    MALLOC_OK = 0,
    MALLOC_ERROR,
};

enum args_status {
    ARGS_OK = 0,
    WRONG_ARGS,
};

#endif //ASSIGNMENT_IMAGE_ROTATION_ERR_STATUS_H
